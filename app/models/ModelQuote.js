import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var ModelQuote = new Schema({  
    iAge : Number,
    iAttainedAge : Number,
    iCid : String,
    iCrossBorderWithoutPass : Boolean,
    iDob : String,
    iEmail : String,
    iFirstName : String,
    iFullName : String,
    iGender : String,
    iLastName : String,
    iNationality : String,
    iOccupation : String,
    iOccupationClass : String,
    iResidence : String,
    iResidenceCityName : String,
    iResidenceCountryName : String,
    iSmoke : String,
    pAge : Number,
    pAttainedAge : Number,
    pCid :String,
    pCrossBorderWithoutPass : Boolean,
    pDob : String,
    pEmail : String,
    pFirstName : Date,
    pFullName : String,
    pGender : String,
    pLastName : String,
    pNationality : String,
    pOccupation : String,
    pOccupationClass : String,
    pResidence : String,
    pResidenceCityName : String,
    pResidenceCountryName : String,
    pSmoke : String,
    paymentMode : String,
    isBackDate : String,
    ccy : String,
    premTerm : String,
    premium : Number,
    sumInsured:Number,
    plans:Object
},
{
    collection: 'posQuoteMst'
});

module.exports = mongoose.model('ModelQuote', ModelQuote);