import mongoose from 'mongoose';
var Schema = mongoose.Schema;

var ModelUserProfile = new Schema({  
    _id : {type: String, required: true},
    userCode : String,
    userCodeDisp :String,
    userName : String,
    authorised : {type: Array,default: []},
    authorisedDes : String,
    channel : String,
    position : String,
    manager : String,
    managerCode : String,
    managerDisp : String,
    compCode : String,
    company : String,
    email : String,
    mobile : String,
    createdDate : { type: Date, default: Date.now },
    lastUpdatedDate : Date
},
{
    collection: 'userProfile'
});

module.exports = mongoose.model('ModelUserProfile', ModelUserProfile);