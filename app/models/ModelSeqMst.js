import mongoose from 'mongoose';
import { date } from 'joi';
var Schema = mongoose.Schema;
var ModelSeqMst = new Schema({
  _id : {type: String, required: true},
  value:Number,
  createDate:{type:Date,default:Date.now}
},
{
    collection: 'sys_seq_mst'
});
module.exports = mongoose.model('ModelSeqMst', ModelSeqMst);