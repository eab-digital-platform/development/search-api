
import mongoose from 'mongoose';
// var mongoose = require('../utils/db.js');
var Schema = mongoose.Schema;
var ModelFNA = new Schema({
  createDate: { type: Date, default: Date.now },
  createBy: String,
  updateDate: { type: Date, default: Date.now },
  updateBy: String,
  EAB: {type:Object},
  CAFE: {type:Object}
},
{
    collection: 'posFNAMst'
});
module.exports = mongoose.model('ModelFNA', ModelFNA);