import mongoose from 'mongoose';
import { date, object } from 'joi';
var Schema = mongoose.Schema;
var ModelRelationMapping = new Schema({
    createBy: String,
    updateBy:String,
    updateDate:{type:Date,default:Date.now},
    EAB: {type:Object},
    currentID:{type:String,required:true},
    relationship:{type:Object},
    createDate:{type:Date,default:Date.now}
},
{
    collection: 'relationshipMapping'
});
module.exports = mongoose.model('ModelRelationMapping', ModelRelationMapping);