/**
 * @module app/index
 */

import express from 'express';
import logger from './loaders/logger';
import route from'./routes/route';
import loader from './loaders';

const app = express();

async function startServer(app) {

    await loader(app);

    logger.info("Server ready. Accepting service...");
}
startServer(app);
export default app;