import config from '../../config/systemConfig';
const amqp = require('amqplib/callback_api');
const _ = require('lodash');
// const log4jUtil = require('../utils/log4j.util');
// const commonUtil = require('../utils/common.util');

// const amqpStr = `amqp://${commonUtil.getConfig('RABBITMQ_USERNAME')}:${commonUtil.getConfig('RABBITMQ_PASSWORD')}@${commonUtil.getConfig('RABBITMQ_URL')}`;
// const amqpStr = `amqp://guest:guest@192.168.225.160:5672`;
const amqpStr = config.rabbitMQconnectionString;

const onError = (error) => {
  if (error) {
    // throw log4jUtil.log('error', `${error}`);
    console.log(error);
  }
};

const onInvalidParameter = (condition, paramValue) => {
  if (!condition) {
    // throw log4jUtil.log('error', `${paramValue}`);
  }
};

const getQueueOption = options => ({
  connection: amqpStr,
  durable: true,
  prefetch: true,
  exclusive: true,
  callback: false,
  ...options,
});

const getFormattedMessage = (message) => {
  switch (typeof message) {
    case 'object':
      return JSON.stringify(message);
    case 'number':
      return message.toString();
    case 'string':
    default:
      return message;
  }
};

const ROLE = {
  PUBLISHER: 0,
  CONSUMER: 1
}

const MODEL = {
  QUEUE: 0,
  EXCHANGE: 1
}

function RabbitMQHelper({
  queue, 
  option = getQueueOption(), 
  role = ROLE.PUBLISHER, 
  model = MODEL.QUEUE, 
  onReceivedMessage = null
}) {
  // ? Parameters
  this.connectionStr = _.get(option, 'connection', null);
  this.queueOption = {
    durable: _.get(option, 'durable', false),
    exclusive: _.get(option, 'exclusive', false),
  };

  // ? Throw
  if (!this.connectionStr) {
    throw new Error('RabbitMQHelper:: Connection not valid:: ', this.connection);
  }
  // ? Constructor
  this.queue = queue || 'general';
  this.channel = null;
  this.connection = null;
  this.role = role;
  this.model = model;
  this.onReceivedMessage = _.isFunction(onReceivedMessage) && this.role === ROLE.CONSUMER ? onReceivedMessage : () => { console.log("MQ | Invalid onReceivedMessage")};
  this.isConnected = false;

  // ? Region should not be callable from outside.
  // #region Callback
  const onCreatedChannel = (error, channel) => {
    // console.log("MQ | onCreatedChannel");
    onError(error);
    onInvalidParameter(channel);

    this.channel = channel;

    // this.channel.assertQueue(this.queue, this.queueOption);

    const isPrefetchable = _.get(option, 'prefetch', false);

    // ? Fair dispatch
    // ? https://www.rabbitmq.com/tutorials/tutorial-two-javascript.html
    // ? don't dispatch a new message to a worker until it has processed and acknowledged the previous one
    if (isPrefetchable) {
      this.channel.prefetch(1);
    }
    this.isConnected = true;
    if (this.role === ROLE.CONSUMER) {
      this.channel.assertQueue(this.queue, {
        durable: _.get(this.queueOption, "durable", false)
      })
      this.channel.consume(this.queue, (message) => {
        // ? You should check your replyTo here
        this.onReceivedMessage(message);
        // if (message.properties.replyTo) {
        //   this.channel.sendToQueue(message.properties.replyTo, getFormattedMessage())
        // }
        this.channel.ack(message);
      });
    }
  };

  const onConnected = (error, connection) => {
    // console.log("MQ | onConnected");
    onError(error);
    onInvalidParameter(connection);

    this.connection = connection;
    connection.createChannel(onCreatedChannel);
  };
  // #endregion

  // #region Functions
  // ! Async, you need to wait it.
  // ! Otherwise, not guaranteed the client and server is connected 
  this.isInitialized = async () => {
    const waitSeconds = (seconds) => new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, seconds);
    });

    while (!this.isConnected) {
      await waitSeconds(1000);
    }
    console.log("MQ | isConnected && isChannelCreated");
    return true;
  };

  // ! OBJECT
  this.sendMessage = async ({
    message, onCallback, queueOption, consumeOption,
  }) => {
    if (this.role === ROLE.PUBLISHER) {
      await this.isInitialized();
      // ? Fallback
      const queueCallback = _.isFunction(onCallback) ? onCallback : () => {};
      // ? Execution
      const newMessage = getFormattedMessage(message);
      this.channel.assertQueue("", this.queueOption, (error, query) => {
        onError(error);
        console.log("MQ | Asserted | ", queue);
        this.channel.consume(query.queue, queueCallback, consumeOption);
        this.channel.sendToQueue(this.queue, Buffer.from(newMessage), {
          ...queueOption,
          replyTo: query.queue
        });
      })
    };
  }

  // ! OBJECT
  this.sendAck = async ({replyTo, acknowledgement, message}) => {
    await this.isInitialized();
    const newAcknowledgement = getFormattedMessage(acknowledgement);
    this.channel.sendToQueue(replyTo, Buffer.from(newAcknowledgement), { correlationId: message.properties.correlationId } )
  }

  this.closeConnection = async () => {
    this.connection.close();
  }
  // #endregion

  // return new Promise((resolve) => {
  amqp.connect(this.connectionStr, onConnected);
  console.log("MQ | Created MQHelper Object")
  // });
}

module.exports = RabbitMQHelper;
module.exports.getQueueOption = getQueueOption;
module.exports.getFormattedMessage = getFormattedMessage;
module.exports.ROLE = ROLE;
module.exports.MODEL = MODEL;
