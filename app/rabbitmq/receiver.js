import config from '../../config/systemConfig';
const MQHelper = require("./rabbitmq.util");
const uuid = require("uuid/v1");
const searchEngineController = require("../controller/searchEngineController");

async function onReceivedMessageCreate(message) {
//    console.log("MQ | Received Message");
   
   /* ??? */
   if (message.properties.replyTo) {
       let data = JSON.parse(message.content);
       let result = await searchEngineController.fnCreate(data.docType,data).catch(error=>{
            result = {"success":false,message:""+error};
        });
        if(!result["success"]){
            result = {"success":false,message:result};
        }
       this.sendAck({ replyTo: message.properties.replyTo, acknowledgement: result, message})
   }
}
async function onReceivedMessageUpdate(message) {
    
    /* ??? */
    if (message.properties.replyTo) {
        let data = JSON.parse(message.content);
       
        let result = await searchEngineController.fnUpdate(data.docType,data.updateCriteria,data).catch(error=>{
            result = {"success":false,message:""+error};
        });
        console.log("MQ | result",result);
        if(!result["success"]){
            result = {"success":false,message:result};
        }
        this.sendAck({ replyTo: message.properties.replyTo, acknowledgement: result, message})
    }
 }

const trial = async () => {
    const rabbitCreate = new MQHelper({
        queue: config.rabbitMQchannelCreate, 
        option: MQHelper.getQueueOption({ durable: false }),
        onReceivedMessage: onReceivedMessageCreate,
        role: MQHelper.ROLE.CONSUMER
    });

    const rabbitUpdate = new MQHelper({
        queue: config.rabbitMQchannelUpdate, 
        option: MQHelper.getQueueOption({ durable: false }),
        onReceivedMessage: onReceivedMessageUpdate,
        role: MQHelper.ROLE.CONSUMER
    });

    // console.log(rabbit);
    // await new Promise((resolve) => {
    //     rabbit.sendMessage({
    //         message: "456",
    //         onCalslback: (message) => {
    //             console.log("MQ | SendQueue | callback ", message.content.toString());
    //             resolve(message);
    //         },
    //         queueOption: { correlationId: uuid() },
    //         consumeOption: { noAck: true }
    //     })
    // })
    // rabbit.closeConnection();
    // process.exit(0);
};
trial();