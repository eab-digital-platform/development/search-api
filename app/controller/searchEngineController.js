import systemConfig from '../../config/systemConfig';
import userConfig from '../../config/userConfig';
import logger from '../loaders/logger';
import mongoose from 'mongoose';
var ModelSeqMst = require('../models/ModelSeqMst');
var ModelFNA = require('../models/ModelFNA');
var ModelUserProfile = require('../models/ModelUserProfile');
var ModelAgentProfile = require('../models/ModelAgentProfile');
const _ = require('lodash');
const dbHelper = require('../utils/dbHelper');
var moment = require("moment");
// function getSystemCollectionRule(searchType){
//   //need to be implement
//   return systemConfig.collectionRules[searchType];
// }
function getUserCollectionRule(searchType){
  //need to be implement
  return userConfig.collectionRules[searchType];
}
function getLoginInfo(){
   //need to be implement
  return {"fullName":"zeng.fanzhong","userCode":"088811","compCode":"01","compSubCode":"AG","agentCode":"000001"};
}
async function replaceFields(modelType,defindedField,parameter){
  const fieldName = defindedField["fieldName"];
  const fieldValue = defindedField["fieldValue"];
  const rule = defindedField["rule"];
  const seqRule = defindedField["seqRule"];
  const source = defindedField["source"];

  const internalInfo = getLoginInfo();

  if(fieldValue.indexOf("{")>=0 && fieldValue.indexOf("}")>=0){
       if(fieldValue=="{generate}"){
          finalFieldValue = rule;
          for(var logininfo in internalInfo){            
            if(finalFieldValue.indexOf("{"+logininfo+"}")>=0){
              finalFieldValue=finalFieldValue.replace("{"+logininfo+"}",internalInfo[logininfo]);
            }
          }
          if(finalFieldValue.indexOf("{seq}")>=0){
            const seqResult = await dbHelper.dbHelpFindOne(ModelSeqMst,{"_id":modelType});
            var seq = 1;
            if(!_.isEmpty(seqResult)){
              seq = seqResult["value"]+1;
            }else{
              var seqMst = new ModelSeqMst({
                 "_id":modelType,
                 "value":1
              });
              await dbHelper.dbHelpSave(seqMst);
            }
            let strSeq ="";
            await dbHelper.dbHelpUpdate(ModelSeqMst,{"_id":modelType},{value:seq});
            if(seqRule["paddingChar"] && seqRule["paddingCNT"]){
              for(let i=0;i<seqRule["paddingCNT"]-seq.toString().length;i++){
                strSeq = strSeq + seqRule["paddingChar"];
              }
            }
            strSeq = strSeq + seq;
            finalFieldValue = finalFieldValue.replace("{seq}",strSeq);
          }
          return {"success":true,result:finalFieldValue};
       }else{
        var finalFieldValue = fieldValue.replace("{","").replace("}","");
        if(source=="loginInfo"){
            if(internalInfo.hasOwnProperty(finalFieldValue)){
                return {"success":true,result:internalInfo[finalFieldValue]};
            }else{
                return {"success":false,message:fieldName+" : No "+finalFieldValue+" definded in Login information"};
            }
          
        }else if(source=="parameter"){
              if(parameter.data.hasOwnProperty(finalFieldValue)){
                return {"success":true,result:parameter.data[finalFieldValue]};
              }else{
                return {"success":false,message:fieldName+" : No "+finalFieldValue+" definded in input Parameter"};
              }

        }
      }
}else{
  return {"success":true,result:fieldValue};
}

};
const  fnSearch =(parameter,queryType) => new Promise((resolve,reject) =>{
  resolve(_fnSearch(parameter,queryType));
});
const fillValueFromObject = function(fieldNameList,fromObject){
  var rtnObject = {};
  if(fieldNameList){
    // logger.debug("fieldNameList=",fieldNameList);
    // logger.debug("fromObject=",fromObject);
    let dateFormat = systemConfig.dateFormat;
    fieldNameList.forEach(showField=>{
      let fieldValue = _.get(fromObject,showField);
      if(showField.indexOf("{datetime}")>=0){
        showField = showField.replace("{datetime}","");
        fieldValue = _.get(fromObject,showField);
        fieldValue = moment(fieldValue).format(dateFormat);
      }else{
        if(_.isDate(fieldValue)){
          fieldValue = moment(fieldValue).format(dateFormat);
        }
      }
      rtnObject = _.set(rtnObject,showField,fieldValue);
    });
  }else{
    rtnObject = fromObject;
  }
  return rtnObject;
}
async function _fnSearch(parameter,queryType){
  var searchType = parameter.docType;
  var searchKey = parameter.searchKey;
  var showSelf = parameter.showSelf;
  var showRelation = parameter.showRelation;
  let queryParams = {};
  let queryAndOrParams= {};
  let orderBy = {};
  let finalResult = [];
  let resultIndex = 0;
  let rpResultSuccess = true;
  
  const searchRule = getUserCollectionRule(searchType);
  const indexFields = searchRule.indexFileds;
  const showFieldsList = _.get(_.get(searchRule,"showFieldsList"),_.get(parameter,"showFieldsList","default"));
  let lookupDesc = _.get(searchRule,"lookupDesc",[]);
  let sortBy = _.get(searchRule,"sortBy",[]);
  const exactMatchFields = searchRule.dependency;
  const orderByConfig = searchRule.orderBy;
  let intexItems = []; 
  let intexItemIndex = 0;
  
  if(indexFields && searchKey){   
    indexFields.forEach((indexField) => {
      let intexItem = {};
      intexItem[indexField] = {'$regex': searchKey, $options: '$i'};
      intexItems[intexItemIndex++]=intexItem;
    })
    queryAndOrParams={$or:intexItems};
  }
  let notInArray = _.get(parameter,"notInArray",[]);
  if(notInArray.length>0){
    queryAndOrParams = _.assign(queryAndOrParams,{"_id":{$nin:notInArray}})
  }
  if (exactMatchFields) {
    for(var exactMatchField of exactMatchFields){
      await replaceFields(searchType,exactMatchField,parameter).then(result =>{
        if(result["success"]){
          queryParams[exactMatchField["fieldName"]]=result["result"];
        }else{          
          finalResult[resultIndex++] = resultIndex+". "+result["message"];
          rpResultSuccess = false;
        }
      });
    }
  }
   if(rpResultSuccess){
      if(parameter.searchStr){
        let searchString =parameter.searchStr;
        if(queryType=="get"){
          searchString = JSON.parse(parameter.searchStr);
        }
        for(var str in searchString){
          queryParams[str] = searchString[str];
        }
      }
      if(orderByConfig){
        orderByConfig.forEach(order=>{
        orderBy[order["orderName"]]=order["order"];
        })
      }
      logger.info("orderBy =",orderBy);
      logger.info("queryParams =",queryParams);
      logger.info("queryAndOrParams =",queryAndOrParams);
      var Model = require('../models/'+searchRule.model);    
      var newStaffs = await dbHelper.searchQuery(Model,queryParams,queryAndOrParams,
        {},
        orderBy).catch(error=>{
          finalResult[resultIndex++]= ""+error;
        });
        if(_.get(newStaffs,"length",0)>0){
          let retrunStaffs = [];
          let lookupDescs = {};
                // logger.debug("lookupDesc = ",lookupDesc);
                for(let i in lookupDesc){
                  // logger.debug("lookupDesc = ",lookupDesc[i]);
                  let lookupModelName = _.get(lookupDesc[i],"fromModel");
                  // logger.debug("lookupModelName = ",lookupModelName);
                  let lookupModel = require("../models/"+lookupModelName);
                  let fromMatch = lookupDesc[i].fromMatch;
                  let lookupCriteria = _.set({},fromMatch.matchField,fromMatch.matchValue);
                  // logger.debug("lookupCriteria = ",lookupCriteria);
                  let loopResult = await dbHelper.searchAllResult(lookupModel,lookupCriteria);
                  lookupDescs = _.set(lookupDescs,lookupDesc[i].localField,loopResult);
            }
            for(let newStaff of newStaffs){
            let staffWithRelation =fillValueFromObject(showFieldsList,newStaff);
            for(let i in lookupDesc){
              let localField = lookupDesc[i].localField;
              let foreignPath = lookupDesc[i].foreignPath;
              let foreignField = lookupDesc[i].foreignField;
              // logger.debug("foreignPath = ",foreignPath);
              let lookupValues = _.head(_.get(lookupDescs,localField));
              // logger.debug("lookupValues = ",lookupValues);
              lookupValues = _.get(lookupValues,foreignPath);
              // logger.debug("lookupValues = ",lookupValues);
              let lookupValue = _.find(lookupValues,_.set({},foreignField,_.get(staffWithRelation,localField)));
              lookupValue = _.get(lookupValue,lookupDesc[i].lookup+".en");
              // logger.debug("lookupValue = ",lookupValue);
              staffWithRelation = _.set(staffWithRelation,lookupDesc[i].as,lookupValue);
            }
            if(showRelation){
              let relationSearchParameter = {};
              relationSearchParameter["docType"]=searchType;
              relationSearchParameter["key"]=""+newStaff._id;
              relationSearchParameter["lang"]=_.get(parameter,"lang","en");
              relationSearchParameter["relationType"]=_.get(parameter,"relationType","");
              logger.debug("relationSearchParameter = ",relationSearchParameter);
              let relation = await _fnRelationalSearch(relationSearchParameter,queryType);
              if(_.get(relation,"success")){
                let familyMember = relation["result"];
                // logger.debug("familyMember result = ",relation);
                if(showSelf){
                  familyMember.forEach(item=>{
                    staffWithRelation =_.concat(staffWithRelation,fillValueFromObject(showFieldsList,item));
                  });
                }else{
                  staffWithRelation[_.get(parameter,"relationType","")] = familyMember;
                }
                
              }
              
            }
            retrunStaffs = _.concat(retrunStaffs,staffWithRelation);
           

          }
          if(sortBy){
            let orderName = [];
            let sortType = [];
            sortBy.forEach(item=>{
              orderName = _.concat(orderName,item.sortName);
              sortType = _.concat(sortType,item.sortType);
            });
            retrunStaffs = _.orderBy(retrunStaffs,orderName,sortType);
          }
          

          finalResult= {"success":true,result:retrunStaffs};
          logger.info("search count = ",newStaffs.length);
        }else{
          finalResult= "No record found!";
        }
    }
  return finalResult;
  };
const  fnRelationalSearch =(parameter,queryType) => new Promise((resolve,reject) =>{
  resolve(_fnRelationalSearch(parameter,queryType));
});
async function _fnRelationalSearch(parameter,queryType){
  var searchType = parameter.docType;
  var grouping = parameter.grouping;
  var showSelf = parameter.showSelf;
  var lang = _.lowerCase(_.get(parameter,"lang","en"));
  // logger.info("lang = ",lang);
  let queryParams = {};
  let finalResult = [];
  let resultIndex = 0;
  let rpResultSuccess = true;  
  const searchRule = getUserCollectionRule(searchType);


  if(rpResultSuccess){
    let relationConfig = searchRule.relationMappingConfig;
    if(relationConfig){
      let relationType = _.get(parameter,"relationType");
      relationConfig = _.find(relationConfig,_.set({},"relationType",relationType))
      let lookupDesc = _.get(relationConfig,"lookupDesc",[]);
      let sortBy = _.get(relationConfig,"sortBy",[]);
      const relationRule = getUserCollectionRule(relationConfig.relationModel);
      const typeDefindedField = _.get(relationConfig,"typeDefindedField");
      const typeDefindedFieldValue = _.get(relationRule,"type");
      queryParams = {};
      let relateModel = require("../models/"+relationRule.model);
      queryParams[relationConfig.refKey]=parameter.key;
      queryParams[typeDefindedField]=typeDefindedFieldValue;
      logger.debug("match = ",queryParams);
      var lookups = relationRule.lookups;
      logger.debug("lookups = ",lookups);
      let returnResult = [];
      let mappingKey = _.get(relationConfig,"mappingKey","");
      if(mappingKey.indexOf(".")>=0){
        mappingKey = mappingKey.split(".")[1];
      }
      if(lookups){
          var localField = lookups[0].localField;
          var subLocalField = lookups[0].subLocalField;
          var finalLocalField = localField;
          var foreignField = lookups[0].foreignField;
          if(subLocalField){
            finalLocalField = _.join([localField,subLocalField],".");
          }
          var as = lookups[0].as;
          var lookup = {

                from: lookups[0].from, // 需要关联的表
                localField: finalLocalField, // product 表需要关联的键
                foreignField: foreignField, // orders 的 matching key
                as: as // 对应的外键集合的数据
              
          };
          logger.debug("lookup = ",lookup);
          logger.debug("queryParams = ",queryParams);
      const newStaffs = await dbHelper.dbHelpAggregate(relateModel,lookup,queryParams).catch(error=>{
          finalResult[resultIndex++]= ""+error;
          rpResultSuccess = false;
        });
        let displayFields = _.get(_.head(lookups),"returnFields",[]);
        // logger.debug("newStaffs = ",newStaffs);
        if(newStaffs.length>0){
          var relationships =_.get(_.head(newStaffs),localField,[]);
          var descriptions =_.get(_.head(newStaffs),as,[]);
          queryParams=[];
            relationships.forEach(relation=>{
              let temp = {};
              temp =_.assign(temp,displayFields.forEach(displayField=>{
                let lable = displayField;               
                if(lable.indexOf("{localField.}")>=0){
                  lable = lable.replace("{localField.}","");                  
                  temp[lable] =relation[lable];                 
                }else if(lable.indexOf("{as.}")>=0){
                  lable = lable.replace("{as.}","");
                  let role = _.get(relation,subLocalField);
                  let option = {};
                  if(foreignField.indexOf(".")>=0){
                   
                    let optionsKey = foreignField.split(".")[0];
                     let options = _.get(_.head(descriptions),optionsKey);
                     let foreignKey = foreignField.split(".")[1];
                     option = _.find(options,{[foreignKey]:role});
                  }else{
                     option = _.find(options,{[foreignField]:role});
                  }
                  temp[lable] =_.get(_.get(option,lable,""),lang,"");
                }
                 return temp;
              }));
              returnResult = _.concat(returnResult,temp);

              if(relationConfig.showOpposit){               
                 queryParams=_.concat(queryParams, _.get(relation,mappingKey));               
              }
            });
          }else{
            rpResultSuccess = false;
          }
      }else{

        const newStaffs =await dbHelper.searchAllResult(relateModel,queryParams);
        // logger.debug("relateModel 1111=",relateModel);
        // logger.debug("queryParams 1111=",queryParams);
        // logger.debug("newStaffs 1111=",newStaffs);
        if(newStaffs.length>0){
          queryParams=[];
          var relationships =_.get(_.head(newStaffs),"relationship",[]);
            relationships.forEach(relation=>{
              returnResult = _.concat(returnResult,relation);
              if(relationConfig.showOpposit){               
                 queryParams=_.concat(queryParams, _.get(relation,mappingKey));               
              }
            });
          }else{
            rpResultSuccess = false;
          }

      }
      if(rpResultSuccess){
            let queryParamsTmp={};
            let finalRnResult = [];
            if(relationConfig.showOpposit){
              queryParamsTmp[_.get(relationConfig,"oppositKey")] = {$in:queryParams};
              // logger.debug("queryParamsTmp 1111=",queryParamsTmp);
              let modelOpposit = require("../models/"+relationConfig.oppositModel);
              const opposits =await dbHelper.searchAllResult(modelOpposit,queryParamsTmp);
              // logger.info("opposits = ",opposits);
              // logger.info("returnResult = ",returnResult);
              if(opposits.length>0){
                let lookupDescs = {};
                // logger.debug("lookupDesc = ",lookupDesc);
                for(let i in lookupDesc){
                  // logger.debug("lookupDesc = ",lookupDesc[i]);
                  let lookupModelName = _.get(lookupDesc[i],"fromModel");
                  // logger.debug("lookupModelName = ",lookupModelName);
                  let lookupModel = require("../models/"+lookupModelName);
                  let fromMatch = lookupDesc[i].fromMatch;
                  let lookupCriteria = _.set({},fromMatch.matchField,fromMatch.matchValue);
                  // logger.debug("lookupCriteria = ",lookupCriteria);
                  let loopResult = await dbHelper.searchAllResult(lookupModel,lookupCriteria);
                  lookupDescs = _.set(lookupDescs,lookupDesc[i].localField,loopResult);
                }
                // logger.debug("lookupDescs = ",lookupDescs);
                returnResult.forEach(item =>{
                  let showOppositFields = _.get(relationConfig,"showOppositFields");
                  let oppositKey = _.get(relationConfig,"oppositKey");
                  let oppositID = _.get(item,mappingKey);
                  // logger.info("oppositID = ",oppositID);
                  // logger.info("oppositKey = ",oppositKey);
                  
                  let condition = {};
                  if(_.eq(oppositKey,"_id")){
                    condition = _.set({},oppositKey,mongoose.Types.ObjectId(oppositID));
                  }else{
                    condition = _.set({},oppositKey,oppositID);
                  }
                  // logger.debug("condition = ",condition);
                  let opposit = _.find(opposits,condition);    
                  // logger.debug("opposit = ",opposit);
                  // logger.debug("showOppositFields = ",showOppositFields);
                  let temp = fillValueFromObject(showOppositFields,opposit);
                  item = _.assign(item,temp);
                  for(let i in lookupDesc){
                    let localField = lookupDesc[i].localField;
                    let foreignPath = lookupDesc[i].foreignPath;
                    let foreignField = lookupDesc[i].foreignField;
                    let fromMatch = lookupDesc[i].fromMatch;
                    // logger.debug("foreignPath = ",foreignPath);
                    let lookupValues = _.head(_.get(lookupDescs,localField));
                    // logger.debug("lookupValues = ",lookupValues);
                    lookupValues = _.get(lookupValues,foreignPath);
                    // logger.debug("lookupValues = ",lookupValues);
                    let lookupValue = _.find(lookupValues,_.set({},foreignField,_.get(opposit,localField)));
                    lookupValue = _.get(lookupValue,lookupDesc[i].lookup+".en");
                    // logger.debug("lookupValue = ",lookupValue);
                    temp = _.set(temp,lookupDesc[i].as,lookupValue);
                  }    
                  finalRnResult = _.concat(finalRnResult,item);
              });
                 
              logger.debug("relation search count = ",opposits.length);
              }
            }
            if(sortBy){
              let orderName = [];
              let sortType = [];
              sortBy.forEach(item=>{
                orderName = _.concat(orderName,item.sortName);
                sortType = _.concat(sortType,item.sortType);
              });
              finalRnResult = _.orderBy(finalRnResult,orderName,sortType);
            }
          if(grouping){
            finalRnResult = _.groupBy(finalRnResult,grouping.groupBy);
            finalRnResult = _(finalRnResult).map((items, iCid) => {
              return {
                groupBy:iCid,
                groupName: _.get(_.head(items),grouping.groupName),
                items: items
              }
            });
          }
          finalResult= {"success":true,result:finalRnResult};
        }
        // else{
        //   finalResult= "No record found!";
        //   rpResultSuccess = false;
        // }
  
    }
}
  return finalResult;
  };
exports.api = {
  //search
  search: function (req, res, next) {
    if (req.query.docType || req.body.docType) {
      let queryType = "";
      let parameter = {};
      if(req.query.docType){
         queryType ="get";
         parameter = req.query;
      }
      else{
          queryType ="post";
          parameter = req.body;
      }
      fnSearch(parameter,queryType).then(vResult =>{ 
         
        if(vResult["success"]){
          res.json({success:true, result: vResult["result"] });
        }else{
          res.json({ success: false, message: vResult});
        }        
      }).catch(error=>{
        res.json({ success: false, message: "Internal server error:"+error});
      });
  }else{
    res.json({ success: false,message: "required parameter:docType Missed"});
  }
  },
//get document by docId
getDoc:async function(req, res, next){
  
  let docId = req.params.docId;
  let queryParams = {"_id":docId};
  // const query = new mongoose.Query();
  let keepGO = true;
  let rtResult = {};

  // logger.info("cols = ",cols);
  for (let model in mongoose.models){
    // logger.info("model = ",model);
    let Model = require('../models/'+model);
    if(keepGO){
      await dbHelper.dbHelpFindOne(Model,queryParams).then(result=>{
        if(result){
          keepGO = false;
          rtResult = {"success":true,result:result};
          if(Model.modelName==='ModelClientProfile'){
            let parameter ={"docType":"clientProfile","searchStr":{"_id":docId},"showRelation":true,"showFieldsList":"default"};
            fnSearch(parameter,"post").then(vResult =>{
              res.json ({"success":true,result:vResult["result"][0]});

            }).catch(error=>{
              res.json({"success":false,message:"Internal server error:"+error});
            });
          }else{
            res.json ({"success":true,result:result});
          }
        }
        }).catch(error=>{
          res.json({"success":false,message:"Internal server error:"+error});
        });
    }
  }
  if(keepGO){
    res.json({"success":false,message:"No record been found!"});
  }
},
 //relationalSearch
 relationalSearch: function (req, res, next) {
  // if (req.body.docType) {
  if (req.query.docType || req.body.docType) {
    var vResult;
    let key = "";
    if(req.query.docType){
      key = req.query.key;
       if(key){
          vResult = _fnRelationalSearch(req.query,"get");
       }
    }
    else{
       key = req.body.key;
       if(key){
        vResult = _fnRelationalSearch(req.body,"post");
       }
    }
    if(key){
    vResult.then((result) => {
      if(result["success"]){
        res.json({ success: true, result: result["result"] });
      }else{
        res.json({ success: false, message: result});
      }
        })
          .catch((error) => {
            res.json({ success: false,message: 'Internal server error:'+ error});
          });
    }else{
      res.json({ success: false,message: "required parameter:key Missed!"});
    }

}else{
  res.json({ success: false,message: "required parameter:docType Missed!"});
}
},  
};
export {
  fnSearch
};