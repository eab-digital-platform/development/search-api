import logger from '../loaders/logger';
var ModelClientProfile = require('../models/ModelClientProfile');
var searchEngineController = require('./searchEngineController');
const dbHelper = require('../utils/dbHelper');
const _ = require('lodash');

var getLoginInfo = function(){
  return {"agentCode":"088314","agentName":"test"};
}

var genCPId = function(){
    return "CP001137-00507";
  };
var checkInputJson = function(input){
    // console.log(input);
    if(input.firstName && input.gender && input.fullName){
        if(!_.isEmpty(input.firstName) && !_.isEmpty(input.gender) && !_.isEmpty(input.fullName)){
            return true;
        }
    }
    return false;
  };
exports.api = {
  //search one
    getCP:function(req, res, next){
    let clientId = req.params.clientId;
    let queryParams = {};
    let queryAndOrParams= {};

    if(clientId){
        queryParams = {"_id":clientId};
    }
    var searchResult = searchEngineController.searchResult("clientProfile",null,queryParams);
    searchResult.then((result) => {
      if(!_.isEmpty(result))
        res.json({ success: true, data: result });
      else
        res.json({ success: true, message: 'No Record Found!' });
  })
    .catch((error) => {
      res.json({ success: false,message: ''+ error});
    });
  },
  //delete  
  deleteCP:function(req, res, next){
    let clientId = req.params.clientId;
    let queryParams = {};
    let queryAndOrParams= {};
    if(clientId){
        queryParams = {"_id":clientId};
    }
    dbHelper.dbHelpRemove(ModelClientProfile,queryParams).then((result) => {
      if(!_.isEmpty(result)){
         res.json({ success: true, result: {"_id":clientId} });
      }
      }).catch((error) => {
          res.json({ success: false,message: 'Client Profile not found'+ error});
        });

  },
  //insert
  createCP:function(req,res,next){
      let clientId = genCPId();
      let modelClientProfile = new ModelClientProfile({
        _id:clientId,
        clientProfile:{
          formId:req.body.formId,
          responseId:req.body.responseId,
          addrBlock:req.body.addrBlock,
          addrCity:req.body.addrCity,
          addrCountry:req.body.addrCountry,
          addrEstate:req.body.addrEstate,
          addrStreet:req.body.addrStreet,
          age:req.body.age,
          agentCode:req.body.agentCode,
          agentId:req.body.agentId,
          allowance:req.body.allowance,
          applicationCount:req.body.applicationCount,
          bundle:req.body.bundle,
          channel:req.body.channel,
          cid:req.body.cid,
          compCode:req.body.compCode,
          dealerGroup:req.body.dealerGroup,
          dependants:req.body.dependants,
          dob:req.body.dob,
          education:req.body.education,
          email:req.body.email,
          employStatus:req.body.employStatus,
          firstName:req.body.firstName,
          fnaRecordIdArray:req.body.fnaRecordIdArray,
          fullName:req.body.fullName,
          gender:req.body.gender,
          hanyuPinyinName:req.body.hanyuPinyinName,
          hasIFAST:req.body.hasIFAST,
          hasUT:req.body.hasUT,
          haveSignDoc:req.body.haveSignDoc,
          idCardNo:req.body.idCardNo,
          idDocType:req.body.idDocType,
          industry:req.body.industry,
          initial:req.body.initial,
          isSmoker:req.body.isSmoker,
          isValid:req.body.isValid,
          language:req.body.language,
          lastName:req.body.lastName,
          lastUpdateDate:req.body.lastUpdateDate,
          lstChgDate:req.body.lstChgDate,
          marital:req.body.marital,
          mobileCountryCode:req.body.mobileCountryCode,
          mobileNo:req.body.mobileNo,
          nameOrder:req.body.nameOrder,
          nationality:req.body.nationality,
          nearAge:req.body.nearAge,
          occupation:req.body.occupation,
          organization:req.body.organization,
          organizationCountry:req.body.organizationCountry,
          othName:req.body.othName,
          otherMobileCountryCode:req.body.otherMobileCountryCode,
          otherNo:req.body.otherNo,
          photo:req.body.photo,
          postalCode:req.body.postalCode,
          referrals:req.body.referrals,
          residenceCountry:req.body.residenceCountry,
          title:req.body.title,
          type:req.body.type,
          unitNum:req.body.unitNum
      }
      });
      
      let validateInputJson = checkInputJson(req.body);
      if(validateInputJson){
        dbHelper.dbHelpSave(modelClientProfile).then(result =>{
        
        if(!_.isEmpty(result)){
            res.json({ success: true, result: {_id:clientId} });
         }else{
            res.json({ success: false, message: "Internal sever error"});
         }
        }).catch((error) => {
        res.json({ success: false, message: ""+error});
         });
    }else{
        return res.status(403).send({
            success: "error",
            message: 'Incorrect body message!'
        });
    }
  },
  //index text search
  textSearch:function(req,res,next){
    if(req.body.searchKey){
        let keyWord = req.body.searchKey;
        dbHelper.textIndexSearch(ModelClientProfile,keyWord).then(result =>{
            
            if(!_.isEmpty(result)){
                res.json({ success: true, data: result });
            }else{
                res.json({ success: false, message: "No record found"});
            }
            }).catch((error) => {
            res.json({ success: false, message: ""+error});
            });
    }else{
        res.json({ success: false, message: "Missing searchKey"});
    }
  }
  
  

}