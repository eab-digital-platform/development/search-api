var express = require('express');
var router = express.Router();
var $userProfile = require('../controller/userProfileControler');
var $clientProfile = require('../controller/clientProfileControler');
var $searchEngine = require('../controller/searchEngineController');

//  router.get('/userProfile/:userId', $userProfile.api.loadUserProfile);
// //Client Profile
// router.get('/getCp/:clientId',$clientProfile.api.getCP);
// router.post('/deleteCp/:clientId',$clientProfile.api.deleteCP);
// router.post('/createCP', $clientProfile.api.createCP);
// router.get('/textSearch', $clientProfile.api.textSearch);
router.get('/getDoc/:docId', $searchEngine.api.getDoc);
router.get('/search', $searchEngine.api.search);
router.get('/relationalSearch', $searchEngine.api.relationalSearch);



module.exports = router;