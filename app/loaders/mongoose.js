import mongoose from 'mongoose';
import {
    Db
} from 'mongodb';
import config from '../../config/systemConfig';

var userName = config.databaseUsername;
var pwd = config.databasePassword;
var url = config.databaseURL;
var databaseName = config.databaseName;

export default async () => { 
    var DB_URL = `mongodb://${userName}:${pwd}@${url}/${databaseName}`;
    const connection = await mongoose.connect(DB_URL, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    });
    return connection.connection.db;
};