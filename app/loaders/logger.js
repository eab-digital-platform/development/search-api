import log4js from 'log4js';

const logger = log4js.getLogger();
log4js.configure({
  appenders: {
    console: {
      type: 'console'
    },
    file: {
      type: 'dateFile',
      filename: './log/system.log',
      compress: true
    }
  },
  categories: {
    default: {
      appenders: ['console', 'file'],
      level: 'debug'
    }
  }
});
log4js.level = 'debug';

var Container = require("typedi").Container;
Container.set("logger", logger);

export default logger;