import mongooseLoader from './mongoose';
import expressLoader from './express';
import logger from './logger';

export default async (app) => {
    logger.info("Loaders :: init :: Starting...");
    await mongooseLoader();
    logger.info('Loaders :: init :: MongoDB loaded.');

    await expressLoader(app);
    logger.info('Loaders :: init :: Express loaded');

    logger.info("Loaders :: init :: Finished.");
}

// export default {
//     init
// };