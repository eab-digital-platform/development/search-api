import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';

var apiRouter = require('../routes/route');
//rabbitMQ
//var mqListener = require('../rabbitmq/receiver');
const {
    errors
} = require('celebrate');

export default async (app) => {
    // Init Helmet security lib
    // Included features by default:
    // 1. dnsPrefetchControl(controls browser DNS prefetching),
    // 2. frameguard(prevent clickjacking),
    // 3. hidePoweredBy (remove the X-Powered-By header),
    // 4. hsts (HTTP Strict Transport Security),
    // 5. ieNoOpen (sets X-Download-Options for IE8+)
    // 6. noSniff (keep clients from sniffing the MIME type)
    // 7. xssFilter(adds some small XSS protections)
    // ref to: https://helmetjs.github.io/docs/

    app.use(cors());
    app.use(helmet());
    app.use(helmet.noCache());

    app.use(morgan('dev'));
    app.use(express.json());
    app.use(express.urlencoded({
        extended: false
    }));
    app.use(cookieParser());
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
      });
    //core-api
    app.use('/search-api', apiRouter);
    //rabbitMQ
    // app.use(mqListener());

    app.use(errors());

    /// catch 404 and forward to error handler
    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err['status'] = 404;
        next(err);
    });

    /// error handlers
    app.use((err, req, res, next) => {
        /**
         * Handle 401 thrown by express-jwt library
         */
        if (err.name === 'UnauthorizedError') {
            return res
                .status(err.status)
                .send({
                    message: err.message
                })
                .end();
        }
        return next(err);
    });
    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.json({
            errors: {
                message: err.message,
            },
        });
    });



}