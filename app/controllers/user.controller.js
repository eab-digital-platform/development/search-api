import { Container } from 'typedi';
import userModel from '../models/user'

async function create(userDTO) {
    const logger = Container.get('logger');
    logger.info('user.Controller :: create');
    try {
        var userRecord = new userModel(userDTO);
        const result = await userRecord.save().catch(err => {
            logger.error(`user.Controller :: create :: error on create: %o`, err.errmsg);
            return { success: false, message: err.errmsg };
        });

        if (result._id) return {success: true};
        else return result;
        
    } catch (e) {
        logger.error(`user.Controller :: create :: error on create: %o`, e);

        // // Throw the error so the process dies (check src/app.ts)
        // throw e;
        return {success: true};
    }
};

export default {
    create
};