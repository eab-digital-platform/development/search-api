# FROM registry.redhat.io/rhoar-nodejs/nodejs-10
#FROM node
FROM node:12.2.0-alpine
EXPOSE 3000:3000
#ENV NODE_ENV production
#ENV CONFIG PRO
#ENV DEV_MODE false
#USER 0
WORKDIR /app
COPY . /app
RUN apk add python make g++
RUN npm install --loglevel error
#RUN npm run dev
CMD ["npm", "run", "dev"]
